import model.Engine
import javax.swing.UIManager

/**
  * @author Alexander Naumov.
  */
object Launcher extends App {
  UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName)
  Engine.start()
}
