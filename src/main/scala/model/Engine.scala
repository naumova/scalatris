package model

import java.awt.Graphics2D


/**
  * @author Alexander Naumov.
  */
object Engine extends Runnable {

  val WIDTH = 300
  val HEIGHT = 565
  val STEP = 30

  private val TITLE = "ScalaTRIS"
  private val CLEAR_COLOR = 0xff181818
  private val NUM_BUFFERS = 3
  private val UPDATE_RATE = 60.0f
  private val UPDATE_INTERVAL = Time.SECOND / UPDATE_RATE
  private val IDLE_TIME = 1
  private var running = false

  private val gameThread:Thread = new Thread(this)

  private val display = Display(WIDTH, HEIGHT, TITLE, CLEAR_COLOR, NUM_BUFFERS)

  val i = new Input
  var z = new Field(i)

  display.addInput(i)

  def start(): Unit = {
    if (running) {
      return
    }
    running = true
    gameThread.start()
  }

  def stop(): Unit = {
    if (!running) {
      return
    }
    running = false
    gameThread.join()
    cleanUp()
  }

  private def update(): Unit = {
    z.update
  }

  private def render(): Unit ={
    display.clean()
    val g = display.bufferGraphics.asInstanceOf[Graphics2D]
    z.render(g)
    display.swapBuffers()
  }

  private def cleanUp(): Unit = {

  }

  override def run(): Unit = {
    z.falling

    var fps = 0
    var upd = 0
    var updl = 0
    var counter: Long = 0
    var lastTime: Long  = Time.get
    var delta: Float = 0

    while (running) {
      val now: Long = Time.get
      var elapsedTime: Long = now - lastTime
      lastTime = now
      counter += elapsedTime
      var rendering = false
      delta += (elapsedTime / UPDATE_INTERVAL)
      while (delta > 1) {
        update()
        upd += 1
        delta -= 1
        if (rendering) {
          updl += 1
        } else {
          rendering = true
        }
      }
      if (rendering) {
        render()
        fps += 1
      } else {
        Thread.sleep(IDLE_TIME)
      }
      if (counter >= Time.SECOND) {
        display.setTitle(TITLE)
        fps = 0
        upd = 0
        updl = 0
        counter = 0
      }
    }
  }
}