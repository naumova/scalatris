package model

/**
  * @author Alexander Naumov.
  */
object Time {
  val SECOND = 1000000000L
  def get: Long = System.nanoTime()
}
