package model

/**
  * @author Alexander Naumov.
  */
object Type extends Enumeration {
  val Type = Value
  val I, O, L, J, S, Z, T = Value
}
