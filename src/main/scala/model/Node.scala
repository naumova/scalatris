package model

import java.awt.{Graphics2D, Rectangle}
import java.awt.image.BufferedImage

import javax.imageio.ImageIO
import model.Rotation._
import model.Engine.STEP

/**
  * @author Alexander Naumov.
  */
case class Node(var b: Point, var color:String, var r: Rotation = A, var isAxis: Boolean = false) {

  val image: BufferedImage = ImageIO.read(classOf[Nothing].getResourceAsStream("/images/" + color + ".png"))

  var active: Boolean = true

  def getTop: Array[Point] = (b.x to b.x + image.getWidth()).map(Point(_, b.y)).toArray

  def getBottom: Array[Point] = (b.x to b.x + image.getWidth()).map(Point(_, b.y + image.getHeight())).toArray

  def rotate(a: Point): Unit = {
    if (!isAxis) {
      if (r == A) {
        if (b.y == a.y) {
          b.y = a.y + (b.x - a.x)
          b.x = a.x
        } else {
          val t = b.x
          b.x = a.x - (b.y - a.y)
          b.y = a.y + (t - a.x)
        }
      }
      if (r == B) {
        if (b.x == a.x) {
          b.x = a.x - (b.y - a.y)
          b.y = a.y
        } else {
          val t = b.y
          b.y = a.y + (b.x - a.x)
          b.x = a.x - (t - a.y)
        }
      }
      if (r == C) {
        if (b.y == a.y) {
          b.y = a.y - (a.x - b.x)
          b.x = a.x
        } else {
          val t = b.y
          b.y = a.y + (b.x - a.x)
          b.x = a.x - (t - a.y)
        }
      }
      if (r == D) {
        if (b.x == a.x) {
          b.x = a.x + (a.y - b.y)
          b.y = a.y
        } else {
          val t = b.y
          b.y = a.y + (b.x - a.x)
          b.x = a.x + (a.y - t)
        }
      }
      changeRotation()
    }
  }

  def down(): Unit = b.y += STEP

  def left(): Unit = b.x -= STEP

  def right(): Unit = b.x += STEP

  private def changeRotation(): Unit = r match {
    case A => r = B
    case B => r = C
    case C => r = D
    case D => r = A
  }

  def render(g: Graphics2D): Unit = g.drawImage(image, b.x, b.y, image.getWidth, image.getHeight, null)

  def getBound: Rectangle = new Rectangle(b.x, b.y, image.getWidth, image.getHeight)
}