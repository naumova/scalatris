package model

import java.awt.Graphics2D
import java.awt.event.KeyEvent.VK_LEFT
import java.awt.event.KeyEvent.VK_RIGHT
import java.awt.event.KeyEvent.VK_DOWN
import java.awt.event.KeyEvent.VK_SPACE

import model.State._
import model.Type._
import model.Engine.HEIGHT
import model.Engine.WIDTH
import model.Engine.STEP


/**
  * @author Alexander Naumov.
  */
class Shape(val t: Type.Value, val c: String, var axis: Point = Point(150, 120)){

  var state: State = ACTIVE

  var body: Array[Node] = create()

  def rotate(i: Input): Unit = {
    if (state == ACTIVE && t != O) {
      body.foreach{
        n => n.rotate(body(0).b)
      }
      shift(body)
      i.foo(false, VK_SPACE)
    }
  }

  def up(): Unit = {
    if (state == ACTIVE) {
      body.foreach(n => n.b.y = n.b.y - STEP)
    }
  }

  def down(i: Input): Unit = {
    if (isDown <= HEIGHT - (STEP + 20) && state == ACTIVE) {
      fall()
      i.foo(false, VK_DOWN)
    } else  {
      state = NOT_ACTIVE
    }
  }

  def left(i: Input = null): Unit = {
    if (maxLeft >= STEP && state == ACTIVE) {
      axis.x -= STEP
      body.foreach(_.left)
      if (i != null) i.foo(false, VK_LEFT)
    }
  }
  
  def right(i: Input = null): Unit = {
    if (maxRight <= WIDTH - STEP * 2 && state == ACTIVE) {
      axis.x += STEP
      body.foreach(_.right)
      if (i != null) i.foo(false, VK_RIGHT)
    }
  }

  def fall(): Unit = {
    if (isDown < HEIGHT - (STEP + 20) && state == ACTIVE) {
      body.foreach(_.down)
      axis.y += STEP
    } else {
      state = NOT_ACTIVE
    }
  }

  def shift(array: Array[Node]): Unit = {
    var a = true
    for (n <- array.indices if a) {
      if (array(n).b.x < 0) {
        right()
        a = false
      } else if (array(n).b.x > WIDTH - STEP) {
        left()
        a = false
      } else if (array(n).b.y > HEIGHT) {
        up()
        a = false
      } else if (array(n).b.y < 0) {
        fall()
        a = false
      }
      if (!a) shift(body)
    }
  }

  private def create(): Array[Node] = ShapeFactory.create(t, c)

  def render(g: Graphics2D): Unit = body.foreach(node => node.render(g))

  def maxLeft(): Int =  body.map(_.b.x).min

  def maxRight(): Int = body.map(_.b.x).max

  def isDown: Int = body.map(_.b.y).max

  def update(i: Input): Unit = {
    if (i.getKey(VK_LEFT)) left(i)
    if (i.getKey(VK_RIGHT)) right(i)
    if (i.getKey(VK_DOWN)) down(i)
    if (i.getKey(VK_SPACE)) rotate(i)
  }
}
