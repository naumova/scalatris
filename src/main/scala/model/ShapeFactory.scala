package model

import model.Type._
import model.Rotation._

/**
  * @author Alexander Naumov.
  */
object ShapeFactory {

  def create(t: Type.Value, color: String): Array[Node] = t match {
      case I => Array(Node(Point(120, 0), color, A, true), Node(Point(150, 0), color), Node(Point(180, 0), color), Node(Point(210, 0), color))
      case O => Array(Node(Point(150, 0), color, A, true), Node(Point(180, 0), color), Node(Point(150, 30), color), Node(Point(180, 30), color))
      case T => Array(Node(Point(150, 0), color, A, true), Node(Point(150, 30), color), Node(Point(180, 30), color), Node(Point(120, 30), color))
      case J => Array(Node(Point(150, 0), color, A, true), Node(Point(180, 0), color), Node(Point(210, 0), color), Node(Point(210, 30), color))
      case L => Array(Node(Point(150, 0), color, A, true), Node(Point(150, 30), color), Node(Point(180, 0), color), Node(Point(210, 0), color))
      case Z => Array(Node(Point(150, 0), color, A, true), Node(Point(180, 0), color), Node(Point(180, 30), color), Node(Point(210, 30), color))
      case S => Array(Node(Point(150, 0), color, A, true), Node(Point(180, 0), color), Node(Point(120, 30), color), Node(Point(150, 30), color))
  }

}
