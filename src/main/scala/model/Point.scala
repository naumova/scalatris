package model

/**
  * @author Alexander Naumov.
  */
case class Point(var x: Int, var y: Int)
