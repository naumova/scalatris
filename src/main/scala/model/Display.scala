package model

import java.awt._
import java.awt.image.{BufferedImage, DataBufferInt}

import javax.swing._
import java.util
import java.awt.image.BufferedImage.TYPE_INT_ARGB
import java.awt.Color.BLACK
import WindowConstants.EXIT_ON_CLOSE
import Font.BOLD

import javax.imageio.ImageIO

/**
  * @author Alexander Naumov.
  */
class Display(val width: Int, val height: Int, val title: String, val _clearColor: Int, val numBuffers: Int) {

  private val created = true

  private val window = new JFrame
  private val rootPanel = new RootPanel
  private val content = new Canvas
  private val next = new JLabel
  private val score = new JLabel
  private val icon = new JLabel
  private val nextLabel = new JLabel
  private val scoreLabel = new JLabel
  private val pauseButton = new JButton
  private val exitButton = new JButton

  private val buttonFont = new Font("Arival", BOLD, 20)

  private val buffer = new BufferedImage(width, height, TYPE_INT_ARGB)
  private val bufferData = buffer.getRaster.getDataBuffer.asInstanceOf[DataBufferInt].getData

  icon.setLocation(318, 9)
  icon.setSize(150, 50)
  icon.setIcon(new ImageIcon(ImageIO.read(classOf[Nothing].getResourceAsStream("/images/title.png"))))

  nextLabel.setLocation(363, 120)
  nextLabel.setSize(60, 30)
  nextLabel.setIcon(new ImageIcon(ImageIO.read(classOf[Nothing].getResourceAsStream("/images/next.png"))))

  scoreLabel.setLocation(355, 320)
  scoreLabel.setSize(75, 30)
  scoreLabel.setIcon(new ImageIcon(ImageIO.read(classOf[Nothing].getResourceAsStream("/images/score.png"))))

  next.setBorder(BorderFactory.createLineBorder(BLACK))
  next.setLocation(318, 150)
  next.setSize(150, 100)
  next.setOpaque(true)
  next.setBackground(new Color(24, 24, 24))

  score.setBorder(BorderFactory.createLineBorder(BLACK))
  score.setLocation(318, 350)
  score.setSize(150, 50)
  score.setOpaque(true)
  score.setBackground(new Color(24, 24, 24))

  pauseButton.setLocation(333, 450)
  pauseButton.setSize(120, 40)
  pauseButton.setFont(buttonFont)
  pauseButton.setText("Pause")

  exitButton.setLocation(333, 515)
  exitButton.setSize(120, 40)
  exitButton.setFont(buttonFont)
  exitButton.setText("Quit")

  content.setSize(width, height)
  content.setLocation(9, 9)

  rootPanel.setSize(width + 177, height + 45)
  rootPanel.setLayout(null)
  rootPanel.add(next)
  rootPanel.add(score)
  rootPanel.add(icon)
  rootPanel.add(nextLabel)
  rootPanel.add(scoreLabel)
//  rootPanel.add(pauseButton)
//  rootPanel.add(exitButton)

  window.setIconImage(ImageIO.read(classOf[Nothing].getResourceAsStream("/images/logo.png")))
  window.setDefaultCloseOperation(EXIT_ON_CLOSE)
  window.setSize(width + 177, height + 45)
  window.setTitle(title)
  window.setResizable(false)
  window.setLayout(null)

  window.getContentPane.setLayout(null)
  window.getContentPane.add(content)
  window.getContentPane.add(rootPanel)
  window.setLocationRelativeTo(null)
  window.setVisible(true)

  val bufferGraphics: Graphics = buffer.getGraphics

  bufferGraphics.asInstanceOf[Graphics2D].setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON)
  content.createBufferStrategy(numBuffers)

  private val bufferStrategy = content.getBufferStrategy

  def clean(): Unit = {
    util.Arrays.fill(bufferData, _clearColor)
  }

  def swapBuffers(): Unit = {
    val g = bufferStrategy.getDrawGraphics
    g.drawImage(buffer, 0, 0, null)
    bufferStrategy.show()
  }

  def setTitle(title: String): Unit = window.setTitle(title)

  def destroy(): Unit = {
    if (!created) {
      return
    }
    window.dispose()
  }

  def getGraphics: Graphics = bufferGraphics

  def addInput(i: Input): Component = window.add(i)
}

object Display {
  def apply(width: Int, height: Int, title: String, _clearColor: Int, numBuffers: Int): Display = new Display (width, height, title, _clearColor, numBuffers)
}

class RootPanel extends JPanel {

  val image: BufferedImage = ImageIO.read(classOf[Nothing].getResourceAsStream("/images/background.png"))

  override def paintComponent(g: Graphics): Unit = g.drawImage(image, 0, 0, null)
}
