package model

/**
  * @author Alexander Naumov.
  */
class Configuration {
  val width: Int = 360
  val height: Int = 560
  val step: Int = 30
}
