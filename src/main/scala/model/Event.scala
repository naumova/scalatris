package model

/**
  * @author Alexander Naumov.
  */
object Event extends Enumeration {
  type Event = Value
  val LEFT, RIGHT, DOWN, ROTATE, STAY = Value
}
