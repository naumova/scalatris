package model

/**
  * @author Alexander Naumov.
  */
object State extends Enumeration {
  type State = Value
  val ACTIVE, NOT_ACTIVE = Value
}
