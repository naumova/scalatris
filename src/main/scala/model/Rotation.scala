package model

/**
  * @author Alexander Naumov.
  */
object Rotation extends Enumeration {
  type Rotation = Value
  val A, B, C, D = Value
}
