package model

import java.awt.event.ActionEvent

import javax.swing.{AbstractAction, JComponent, KeyStroke}

/**
  * @author Alexander Naumov.
  */
class MyInput extends JComponent {

  val map: Array[Boolean] = new Array[Boolean](256)

  def fun: Int => Unit = (i: Int) => {
    getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(i, 0, false), i * 2)
    getActionMap.put(i * 2, new AbstractAction() {
      override def actionPerformed(e: ActionEvent): Unit = {
        map(i) = true
      }
    })

    getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(i, 0, true), i * 2 + 1)
    getActionMap.put(i * 2 + 1, new AbstractAction() {
      override def actionPerformed(e: ActionEvent): Unit = {
        map(i) = false
      }
    })
  }

  map.foreach(e => fun(map.indexOf(e)))

  def getKey(keyCode: Int) = map(keyCode)

  def foo(b: Boolean, i: Int): Unit = map(i) = b
}
