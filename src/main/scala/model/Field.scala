package model

import java.awt.Graphics2D
import scala.util.Random
import model.State._


/**
  * @author Alexander Naumov.
  */
class Field(val input: Input) {

  val delay: Long = 1000L
  val period: Long = 1000L
  var nodes: List[Node] = List.empty

  var currShape: Shape = _
  var nextShape: Shape = _

  if (currShape == null) {
    currShape = newShape
  }

  def render(g: Graphics2D): Unit = {
    nodes.foreach(_.render(g))
    currShape.render(g)
  }

  def update: Unit = {
    coll
    currShape.update(input)
    if (currShape.state == State.NOT_ACTIVE) {
      currShape.body.foreach(n => nodes = n :: nodes)
      currShape = newShape
    }
  }

  def falling: Unit = {
    if (currShape.state == ACTIVE) {
      val t = new java.util.Timer()
      val task = new java.util.TimerTask {
        def run() = currShape.fall
      }
      t.schedule(task, delay, period)
    }
  }

  def newShape: Shape = new Shape(Field.randomType, Field.randomColor)

  def coll: Unit = {

  }

}

object Field {

  val colors = Array("blue", "green", "orange", "yellow", "red", "violet", "white")

  def randomColor: String = {
    val r = new Random
    colors(r.nextInt(colors.length))
  }

  def randomType = {
    val r = new Random
    val types = Type.values.toArray
    types(r.nextInt(types.length - 1) + 1)
  }

}
