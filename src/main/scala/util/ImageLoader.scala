package util

import java.awt.image.BufferedImage

import javax.imageio.ImageIO

/**
  * @author Alexander Naumov.
  */
object ImageLoader {
  def load(name: String): BufferedImage = {
    val image = ImageIO.read(classOf[Nothing].getResourceAsStream(name))
    image
  }
}
